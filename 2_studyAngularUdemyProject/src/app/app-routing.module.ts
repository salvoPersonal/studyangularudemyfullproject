import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingListComponent } from './components/shopping-list/shopping-list.component';
import { RecipesDetailComponent } from './components/recipes/recipes-detail/recipes-detail.component';
import { RecipesStartComponent } from './components/recipes/recipes-start/recipes-start.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { RecipesEditComponent } from './components/recipes/recipes-edit/recipes-edit.component';

const routes: Routes = [
	{ path: '', redirectTo: "/recipes", pathMatch: "full" },
	{
		path: 'recipes', component: RecipesComponent, children: [
			{ path: '', component: RecipesStartComponent },
			/* le prossime 2 rotte con path: 'new', e path: ':id', l'ordine deve essere prima quello con new perche è una stringa statica(new)
			 e successivamente quella con :id, perche altrimenti il patch matchato dato che :id è dinamico sara sempre quello, indipendentemente da cosa passeremo nell'url 
			*/
			{ path: 'new', component: RecipesEditComponent },
			{ path: ':id', component: RecipesDetailComponent },
			{ path: ':id/edit', component: RecipesEditComponent }
		]
	},
	{ path: 'shopping-list', component: ShoppingListComponent },
	{ path: '**', redirectTo: "/recipes" }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
