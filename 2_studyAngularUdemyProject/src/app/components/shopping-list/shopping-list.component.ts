import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ShoppingListService } from 'src/app/services/shoppingList/shopping-list.service';
import { CIngridient } from 'src/app/shared/CIngredient/CIngridient.model';

@Component({
	selector: 'app-shopping-list',
	templateUrl: './shopping-list.component.html',
	styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
	ingridients: CIngridient[] = []
	private subscription$: Subscription = new Subscription();

	constructor(private shoppingListService: ShoppingListService) { }

	ngOnInit(): void {
		this.ingridients = this.shoppingListService.getIngridients();
		this.subscription$ = this.shoppingListService.ingridientsChange.subscribe(
			(ingridients: CIngridient[]) => { this.ingridients = ingridients }
		);
	}

	ngOnDestroy(): void {
		this.subscription$?.unsubscribe();
	}
}
