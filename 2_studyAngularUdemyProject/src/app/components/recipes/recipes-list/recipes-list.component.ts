import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CRecipe } from '../../../shared/CRecipe/CRecipe.model';
import { RecipeService } from 'src/app/services/recipe/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-recipes-list',
	templateUrl: './recipes-list.component.html',
	styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
	recipes: CRecipe[] = [];

	constructor(
		private recipeService: RecipeService,
		private router: Router,
		private route: ActivatedRoute
	) {

	}
	ngOnInit(): void {
		this.recipes = this.recipeService.getAllRecipes();
	}

	onNewButton() {
		this.router.navigate(['new'], { relativeTo: this.route })
	}
}
