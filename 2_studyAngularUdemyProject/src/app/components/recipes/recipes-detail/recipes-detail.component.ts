import { Component, Input, OnInit } from '@angular/core';
import { CRecipe } from '../../../shared/CRecipe/CRecipe.model';
import { RecipeService } from 'src/app/services/recipe/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-recipes-detail',
	templateUrl: './recipes-detail.component.html',
	styleUrls: ['./recipes-detail.component.css']
})
export class RecipesDetailComponent implements OnInit {
	recipeSelected!: CRecipe
	recipeId: number = 0;

	constructor(
		private recipeService: RecipeService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit(): void {
		this.route.params.subscribe((params) => {
			this.recipeId = +params["id"]
			const allRecipes = this.recipeService.getAllRecipes();
			this.recipeSelected = allRecipes[this.recipeId]
		})
	}

	onAddRecipeShoppingList() {
		this.recipeService.addIngridientsInShoppingList(this.recipeSelected.ingridients);
	}

	onClickEditButton() {
		this.router.navigate(["edit"], { relativeTo: this.route });
		// path piu complresso saliamo di un livello per questo dobbiamo specificare l'id 
		// this.router.navigate(["../", this.recipeId, "edit"], { relativeTo: this.route }) 

	}

}
