import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';

@Component({
	selector: 'app-recipes-edit',
	templateUrl: './recipes-edit.component.html',
	styleUrls: ['./recipes-edit.component.css']
})
export class RecipesEditComponent implements OnInit {
	id: number = 0;
	editMode: boolean = false;

	constructor(private route: ActivatedRoute) { }
	ngOnInit(): void {
		this.route.paramMap.subscribe((params: Params) => {
			this.id = +params["id"];
			this.editMode = params["id"] != null;
			console.log(this.editMode)
		})
	}



}
