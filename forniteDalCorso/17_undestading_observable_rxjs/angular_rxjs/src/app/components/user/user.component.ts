import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
	userId: number = 0;
	/*
	Un subject e diverso da un observable passivo al quale ci si puo sottoscrivere.
	Un subject pupo chiamare il prossimo elemento dall'esterno, il che lo rendeperfetto per utilizzarlo come eventEmitter.
	Quindi se non abbiamo una fornte di eventi passiva come una richiesta http o eventi del Dom abbiamo qualcosa per 
	triggerare l'attivazione del prossimo elemento.
	E consignliato non usare Event emitter ma subject, perchè sono un piu efficenti, e con tutti gli operatori perche un subject 
	alla fine e una  specie di osservabile è un enorme vantaggio, utilizzare i subject invece degli osservabili. 
	Una nota importate: come per gli observable, dovresti effettuare l'unsubscrivbe ai subject quando non ti importano piu.
	IL modo per effettuare l'unsubscribe è uguale a quello degli observable, salvati la sottoscrizione e richhiamala 
	nel metodo onDestroy di angular.
	Questo vale solo se li usi come emettitori di eventi trasversali tra componenti, non si usano subject al posto di 
	eventEmitter quando si usa @Output
	
	Si usano i subject per comunicare attraverso componenti, non padre-figlio, che usano un service. 
	Se non si sottoscrive ad un eventEmitter probabilmete si tratta di un output, se fai la sottoscrizione manuale è un Subject. 

	*/
	activatedEmitter = new Subject<boolean>();
	userActivated: boolean = false;
	private activatedEmitter$: Subscription = new Subscription();

	constructor(private router: Router, private route: ActivatedRoute) { }

	ngOnInit(): void {
		this.route.params.subscribe((params: Params) => {
			this.userId = +params["id"]
		})
		/* 
		Ogni volta che viene emesso un dato la nostra sottoscrizione lo saprà.
		In questo caso params è l'osserable, e lo strea è il parametro nella rotta ogni volta che andiamo nell'url e cambia 
		in questa funzione di subscribe noi otteniamo il nuovo param in questo caso l'id.
		Angular ha gia degli observable predisposti che non sara necessario creare a mano, params ne è un esempio. 
		*/

		this.activatedEmitter$ = this.activatedEmitter.subscribe(
			(isUserActivated: any) => { this.userActivated = isUserActivated }
		)
	}

	OnclickHome() {
		this.router.navigate(['']);
	}

	OnclickUser(id: number) {
		this.router.navigate(["/user", id]);
	}

	onActivate() {
		this.activatedEmitter.next(true);
	}

	ngOnDestroy(): void {
		this.activatedEmitter$?.unsubscribe();
	}
}
