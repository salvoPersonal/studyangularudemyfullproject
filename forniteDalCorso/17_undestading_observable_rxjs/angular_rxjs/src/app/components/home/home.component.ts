import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Observer, Subscription, filter, interval, map } from 'rxjs';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

	private homeSubScribe$: Subscription = new Subscription();
	private customSubscription$: Subscription = new Subscription();

	constructor(private router: Router) { }

	ngOnInit(): void {
		/*
		this.homeSubScribe$ = interval(1000).subscribe, andioamo a memorizzare quello che restiuisce il subscribe, cioe una subscription. 

		*/

		// this.homeSubScribe$ = interval(1000).subscribe((count: number) => {
		// 	console.log(count)
		// })

		/*
		se non passiamo nulla rxjs passera observer per noi. questo argomento e chiamato observer, ma cosa è un observer ?
		l'observer è la parte informata sui dati, riguardo gli errori i nuovi dati o il completamento dell'osservable.
		Non siamo responsabili dell'ascolto perchè L'observable è anche l'observer(l'ascoltatore), dobbiamo solo dirgli 
		quando emettere nuovi dati.
		*/

		/* 
		il observer ha 3 metodi che possono essere richiamati e sono quelli per emettere i dati di un observable 
		*/

		const customIntervalobservable = Observable.create((observer: any) => {
			let count = 0;
			setInterval(() => {
				if (count === 5) {
					/*
					Questo è come lanciare un completare un observable, è un operazione normale per gli Observable.
					Quando un observable è completato non emettara piu valori, per questo il metodo observer.error() non 
					viene richiamato
					*/
					observer.complete()
				}
				if (count > 3) {
					/* Questo è come lanciare un errore un observable*/
					observer.error(new Error("Observable is great than 3"))
				}
				observer.next(count);
				count++;
			}, 1000)
		})

		/*
		Tutti gli observable hanno il metodo pipe importato da rxjs.
		Osserva come dentro il metodo pipe usiamo data seguendo la stessa struttura del .subscribe sopra. 
		Il metodo pipe non cambia i dati dell'observable. Per questo alla seconda sottoscrizione funziona ancora tutto 
		come prima, andra a cambiare i dati all'interno di quella pipe. 
		Se vogliamo modificare i dati del subscribe dobbiamo usare il metodo pipe prima di Subscribe.
		Se ci sono piu operatori si possono mettere tutto nel metodo pipe, dato che accetta un numero illimatato di argomenti
		e ogni agromento è un operatore importato da rxjs, e vengono eseguiti uno dopo l'altro.
		Il potere negli operatori sta nel fatto di poter costrire catena di passaggi in cui far passare i nostri flussi di dati(data flows),
		e ci aiuta quando dobbiamo transofrmare i dati, filtrarli e cosi via ...
		*/


		this.customSubscription$ = customIntervalobservable.pipe(
			filter((data: number) => { return data > 0 }),
			map((data: number) => {
				return 'Round:' + (data + 1)
			})
		).subscribe((data: any) => {
			console.log(data);
		}, (error: Error) => {
			/*
			usiamo il secondo argomento del metodo unsubscrive cioe error, la funzione che indica cosa fare 
			se la sottoscrizione ritorna un errore.  
			PS. nota che il messaggio di errore adesso e un console.log() nel browser.
			 */
			console.log(error)
		}, () => {
			console.log("completed");
			/* 
			la terza funzione e quella del complete, questa funzione non passa argomenti
			La funzione complete non si attiva anche quando l'observable va in errore, perche un 
			Errore annulla l'osservabile, non lo completa
			*/

		})
		/*
		Se esco dalla pagina home e vado nella pagina utente il nostro observable continua a contare, questo è un 
		apsetto che tenuto in considerazione, gli observable non smettono di emettere valori solo perche non sono piu 
		interessati(nel componente ), bisogna fermare l'emissione dei valori degli osservabili se non siamo piu interessati 
		a loro valore, altrimenti si va a creare un memory leak, (sovraccarico della memoria). 
		Se ad esempio abbiamo gia un osservabile in esecvuzione e torniamo nella pagina home esempio dalla pagina user, 
		si andra a creare un nuovo observable, e se iteriamo questa azione se ne creerà un altro ancora, andando ad 
		esaurire rapidamente le risorse a disposizione e causando un rallentamento dell'applicazione e infine 
		un memory leak . 
		Per risolvere questo problema dobbiamo salvare in delle variaibli le sottoscrizioni agli observable sottoscrizioni
		e annullare la subscription tramite il metodo unsubscribe quando usciamo dal componente.
		*/

		/*
		L'emissione dei dati nel 90% delle volte è quello che ci interessa degli observable.
		Quando si tratta di una richiesta http si deve gestire anche la parte di gestione degli errori, quando 
		si verifica un errore in un Observable smette di emettere dati, possiamo anche richiamare il metodo di errore 
		tramite observable.error(), quando si verifica un errore dato che l'observable si ferma( termina), non è necessario effettuare l'unsubscribe.
		*/

		/*
		Ogni volta che si effettua un subscribe impostiamo le diverse funzioni di rxjs, il nostro observable ci fornira:
		i dati , gli errori, ecc...
		Si costruiscono i propi observable, e gli observable forniti da angualr come parmas, e piu o meno tutti gli observable 
		funzionano cosi: avvolgono un evento, che si tratti di un intervallo, una richiesta ajax , o un clickListener, 
		ritorneranno: dei dati, un errore o il completamento dell'observable, e si useranno sopratutto con funzioni che gestiranno
		questi 3 casi. 
		*/





	}

	ngOnDestroy(): void {
		this.homeSubScribe$?.unsubscribe();
		this.customSubscription$?.unsubscribe();
		/*
		utilizzare onDestroy sugli observable previene i memory leak(perdita di memoria), andando a eliminare la scuscription 
		ogni volta che usciamo dalla pagina home, per gli observable gia presenti in angular non sara necessario 
		effettuare l'unsubscribe perche sara Angular a farlo per noi, ad esempio l'observable params nel componente user.
		*/

		/*
		anche se l'observable è completato il nostro subscribe potrebbe non saperlo, ecco perche è possibile eseguire 
		l'unsubscribe senza ricevere errori. 
		*/
	}


	OnclickHome() {
		this.router.navigate(['']);
	}

	OnclickUser(userId: number) {
		this.router.navigate(['/user', userId]);
	}


}

