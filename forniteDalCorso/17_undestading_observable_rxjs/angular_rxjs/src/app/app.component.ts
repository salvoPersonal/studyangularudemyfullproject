import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'angular_rxjs';
}

/*
aggiunto al progetto angualr rxjs-compact, tramite il coamdno npm: npm install --save rxjs-compat
*/

/*
Cosa è un observable? 
Un osservable puo essere considerato una fonte di dati, in angular è un oggetto che importiamo da rxJs. 
Gli obserbale implementi seguono l'observable pattern, abbiamo un observable e un observer e nel mezzo abbiamo lo 
stream(la timeline), in questo stream possiamo trovare dati multi o pacchetti a seconda dell'observable.
Un observable può emettere i dati in perchè l'utente lo triggera(attiva), in modo porgramamtico o puè essere connesso a:
1) un bottone, ogni volta che faccio click emetto un pacchetto in automatico, 
2) un servizio http che lo connette con la richiesta http, e quando abbiamo la response(risposta http), essa sara enemssa 
dall'observable come dati.  
L'observer è il nostro codice che si sottoscrive all'observable.
Abbiamo 3 modi per gestire i dati di un observable:
1) gestione dati: 
2) gestione errori:
3) gestione completamento dell'observable:
Nei box (foto 1_rxjs observable e observer) il nostro codice sara eseguito.
Ci sono Observable che non si completeranno mai, ad esempio quelli che sono legati ad un bottone, perche l'utente puo 
cliccare su di esso quante volte vuole e noi non possiamo sapere quando l'observable sara completato, altre oBservable 
ad esempio quello agganciato alla richeista http sara completato non appena la risposta arriverà, perche cos'altro 
dovrebbe accadere, la risposta è arrivata, è andato/fatta/finita.  
Cosi funziona generalmente il pattern degli observable e naturalemte lo si usa per gestire attivita asincrone, perchè 
tutte le fonti di dati( trigger eventi utenti, richieste http) sono attività asincrone non si sa quando l'utente le 
attivera e nemmeno quato tempo ci vorra per eseguirle.
Qunidi si esegue il codice dell'applicazione normalmente senza dover attende la risposta o il completamente dell'observable,
perche cio bloccherebbe il programma e la logica. 
Quindi ci serve un modo per gestire i task asyncroni e possiamo usare async e await non e del tutto male usarli,
ma esiste un altro approccio diverso. 
Gli observable hanno un grande vantaggio gli operatori. 

*/

/* 
Gli observable non fanno parte di javascript o typescript ma da una libreria esterna rxJs per questo 
la ritrovi nel file package.json  
*/

/* 
Gli operatori sono una caratteristica magica di rxjs, loro transofrmano un observable in costrutti fantastici. 
Se abbiamo un observable e un observer, otteniamo i dati con la sottoscrizione, a volte si vogliono filtrare i dati 
dell'observable e invece di farlo nella funzione o sottofunzione, esiste un modo piu elegante, invece di usare le funzioni
possiamo usare gli operatori nel mezzo tra il nostro observable e l'observer, cioè gli operatori eseguendo delle cose 
sui dati e poi si sottoscrivono i risultati dell'observable( foto 2_rxJs_schema_before_operator).
Gli operatori diventano importanti quando non abbiamo libero accesso al codice di create dell'observable e agiscono nel 
subscribe a quell'observable
*/