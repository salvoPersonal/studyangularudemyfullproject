import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-servers',
	templateUrl: './servers.component.html',
	styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
	public servers: { id: number, name: string, status: string }[] = [];

	constructor(private serversService: ServersService, private router: Router, private route: ActivatedRoute) { }

	ngOnInit() {
		this.servers = this.serversService.getServers();
	}

	onRealod() {
		this.router.navigate(['servers'])
		/*
			// qui possiamo anche usare il path relativo perche il router non conosce la posizione di dove siamo, 
			// in quale componente, possiamo passare un secondo argomento che indica l'esatto punto di navigazione,
			ad esempio usando {relativeTo:}, questo link sara sempre il dominio del root, cioe locahost:4200 , 
			non passiamo una stringa ma un percorso che possiamo iniettare, possiamo ottenere la rotta attuale
			injecttando ActivatedRoute, è una solrta di oggetto javascript che conserva metadati e oggetti complessi 
			sulla rotta attualemtne attiva. 
			Tramite questo pezzo di informazione angualr sa qual è il percorso attualemtne attivo, e da questo punto qui 
			si risolvono tutti i percorsi relativi, per questo l'applciazione si rompe se adesso proviamo a navigare con 
			questo codice : 	this.router.navigate(['servers'], { relativeTo: this.route })

		*/
	}
}
