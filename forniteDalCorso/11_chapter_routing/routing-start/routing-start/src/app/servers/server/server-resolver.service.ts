import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServersService } from "../servers.service";

export interface Server {
	id: number, name: string, status: string
}

@Injectable()
export class ServerResolver implements Resolve<Server> {
	constructor(private serverService: ServersService) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Server | Observable<Server> | Promise<Server> {
		return this.serverService.getServer(+route.params['id'])
	}

	/*
	Ci serve pcaricare i dati di questo compoennte in anticipo, questo funzionerebbe anche con un Observable o una Promise 
	con del codice asincrono. 
	*/


}

/*
un resolver server pre precaricare un componente e caricarlo alla fine dopo che le verifiche sulle rotte 
sono state eseguite e che i canActive vanno bene. 
Il tipo di un resolver è il tipo di dato che vogliamo ci ritorni in questo caso un server. 
Alla fine il metodo resolve deve ritornare un Observable del tipo deniti sopra dalla classe cioe un server. 
*/ 