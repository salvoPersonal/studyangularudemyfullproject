import { Component, OnInit } from '@angular/core';
import { ServersService } from '../servers.service';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';

@Component({
	selector: 'app-server',
	templateUrl: './server.component.html',
	styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
	server: { id: number, name: string, status: string };

	constructor(private serversService: ServersService, private route: ActivatedRoute, private router: Router) { }

	ngOnInit() {
		this.route.data.subscribe((data: Data) => {
			this.server = data['server']
		})

		/*
		let serverId = +this.route.snapshot.params['id']
		this.server = this.serversService.getServer(serverId);
		this.route.params.subscribe((params: Params) => {
			// serverId = Number(params['id']) Cast esplicito da stringa a numero 
			// in questo caso l'id provenendo dall'url sara di tipo stringa, perche l'url è una stringa 
			// quindi dobbiamo convertire l'id in numero e possiamo farlo o con il cast esplicito o aggiungendo + 
			// prima della stringa 
			this.server = this.serversService.getServer(+params.id);
		});
		*/
	}

	onEdit() {
		this.router.navigate(['edit'], { relativeTo: this.route, queryParamsHandling: 'preserve' })
		// rotta relativa perche angular sa quale percorso vogliamo navigare, ovvero appendere /edit alal fine
		// del percorso attuale
		// al secondo parametro di navigate è possibile specificare nell'oggetto anche 
		// se non specifichiamo nulla i queryparams saranno azzerati 
		// queryParamsHandling(esempio { relativeTo: this.route,queryParamsHandling:'merge'}), per mergiare i nuovi queryparams 
		// altrimenti possiamo usare preserve per preservare(mantenuti) quelli vecchi, in questo caso se ne generiamo di nuovi queryparams
		// quelli vecchi andranno a sovrascrivere i nuovi, in questo caso dovremmo usare merge

	}
}
