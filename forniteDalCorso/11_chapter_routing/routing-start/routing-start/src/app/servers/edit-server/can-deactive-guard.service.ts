import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

export interface CanComponentDeactive {
	canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}

export class CanDeactiveGuard implements CanDeactivate<CanComponentDeactive> {
	canDeactivate(component: CanComponentDeactive, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		return component.canDeactivate();
		/*
		adesso angular potra richiamare il metodo canDeactivated da un servizio e potra sapre se si puo partire o meno,
		perche abbiamo bisogno di questa guardia 
		*/
	}

}