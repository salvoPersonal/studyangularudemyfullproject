import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, CanDeactivate, Params, Router } from '@angular/router';
import { CanComponentDeactive } from './can-deactive-guard.service';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-edit-server',
	templateUrl: './edit-server.component.html',
	styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit, CanComponentDeactive {
	server: { id: number, name: string, status: string };
	serverName: string = '';
	serverStatus: string = '';
	allowEdit: boolean = false;
	changeSaved: boolean = false;

	constructor(private serversService: ServersService, private route: ActivatedRoute, private router: Router) { }


	ngOnInit() {
		console.log('queryParams: ', this.route.snapshot.queryParams)
		console.log('fragment: ', this.route.snapshot.fragment)
		this.route.queryParams.subscribe((params: Params) => {
			this.allowEdit = params['allowEdit'] === '1' ? true : false;
		})
		this.route.fragment.subscribe()
		/*
		Propio come per params possiamo sottoscriverci a queryParams e fragment sono due osservabili,
		come per params il destroy sara gestito anche qui da angular.
		Oppure possiamo usare lo snapshot se all'interno del nostro componente non richimere se stesso,
		perche lo snashot avviene quando il compoennte viene creato
		*/
		this.server = this.serversService.getServer(1);
		this.serverName = this.server.name;
		this.serverStatus = this.server.status;
	}

	onUpdateServer() {
		this.serversService.updateServer(this.server.id, { name: this.serverName, status: this.serverStatus });
		this.changeSaved = true;
		this.router.navigate([' ../'], { relativeTo: this.route })
	}

	canDeactivate(): boolean | Promise<boolean> | Observable<boolean> {
		if (!this.allowEdit) {
			return true
		}

		if ((this.serverName !== this.server.name || this.serverStatus !== this.server.status) && !this.changeSaved) {
			return confirm("DO you want to discard the changes?");
		} else {
			return true;
		}
	};

}
