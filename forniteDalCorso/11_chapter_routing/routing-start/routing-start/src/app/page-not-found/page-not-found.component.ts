import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
	selector: 'app-page-not-found',
	templateUrl: './page-not-found.component.html',
	styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
	errorMessage: string
	constructor(private route: ActivatedRoute) { }

	ngOnInit(): void {
		// this.errorMessage = this.route.snapshot.data['message'];
		this.route.data.subscribe(
			(data: Data) => this.errorMessage = data['message']
		)

		/*
		funzionano entrambi come per tutti i subscribe su this.route, il secondo metodo è sempre quello migliore ma 
		funziona anche usando lo snapshot perche non ci interessa richiamare questo componente da se stesso.
		Il tipo di dati che definiamo sara di tipo data, come per gli altri parametri di this.route 
		*/

	}

}
