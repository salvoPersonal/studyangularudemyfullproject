import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	constructor(private router: Router, private authService: AuthService) { }

	ngOnInit() {
	}

	onLoadServer(id: number) {
		// complex calculation
		this.router.navigate(["/servers", id, 'edit'], { queryParams: { allowEdit: '1' }, fragment: 'loading' })
		// equivale al vantaggio di attivarlo da html con routerLink=['servers','']

		// in this.router.navigate(), dopo il primo parametro che è un array possiamo passare un oggetto ,
		//  dove possiamo specificare 2 propieta queryParams , fragment per passare i dati ( esempio ?=allowEdit=1#loading)
	}

	onLogin() {
		this.authService.login()
	}
	onLogout() {
		this.authService.logout()
	}

}
