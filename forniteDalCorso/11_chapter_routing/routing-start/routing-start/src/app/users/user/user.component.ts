import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
	user: { id: number, name: string };
	paramSubscription: Subscription

	constructor(private route: ActivatedRoute) { }


	ngOnInit() {
		this.user = {
			id: this.route.snapshot.params['id'],
			name: this.route.snapshot.params['name'],
		}

		this.paramSubscription = this.route.params.subscribe((params: Params) => {
			this.user.id = params['id'];
			this.user.name = params['name'];
		})

		/*
		Angular gestira la unsubscribe di questo osservabile ogni volta che si distruggera il componente, lo farà per noi.
		L'osservabile non e strettamente legato ad un componente ma è in memoria indipendete da esso.
		Possiamo aggiungerlo manualemtne aggiungendo l'interfaccia onDestroy al camponente e all'interno del metodo onDestroy
		effettuare l'unsubscription a quell'osservabile, è importate da fare con i nostri osservabili,
		per adesso questo osservabile è gestito da angualar.
		Aggiungere l'unsubscription a mano sui params non influira in nulla di negativo.
		*/
	}


	ngOnDestroy(): void {
		this.paramSubscription.unsubscribe()
	}
	/*
	uno dei dati contenuti nell'oggetto ActivateRoute è l'utente attualemtne attivo, quindi in questo caso ci dara accesso 
	all'id dell'utnete passato.
	lo snapshaot è l'isntate di quella rotta, this.route.snapshot.params['id'] , cosi possiamo accedere alle propieta di quella rotta 
	che sono state definite nei parametri del percorso. 
	Possiamo recuperare l'id dall'oggetto params 
	*/

}
