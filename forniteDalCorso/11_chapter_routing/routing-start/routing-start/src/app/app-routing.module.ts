import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { EditServerComponent } from "./servers/edit-server/edit-server.component";
import { ServerComponent } from "./servers/server/server.component";
import { ServersComponent } from "./servers/servers.component";
import { UserComponent } from "./users/user/user.component";
import { UsersComponent } from "./users/users.component";
import { AuthGuard } from "./auth-guard.service";
import { CanDeactiveGuard } from "./servers/edit-server/can-deactive-guard.service";
import { ErrorPageComponent } from "src/error-page/error-page.component";
import { ServerResolver } from "./servers/server/server-resolver.service";

const appRoutes: Routes = [
	{ path: '', component: HomeComponent },
	{
		path: 'users', component: UsersComponent, children: [
			{ path: ':id/:name', component: UserComponent },
		]
	},
	/*
	:id nel routing indica un paramentro 
	*/
	{
		path: 'servers',
		// canActivate: [AuthGuard],
		canActivateChild: [AuthGuard],

		component: ServersComponent, children: [
			{ path: ':id', component: ServerComponent, resolve: { server: ServerResolver } },
			/*
			un resolver viene usato tramite un oggetto chaive => valore, il nome che possiamo usare per le chiavi possiamo sceglierlo noi,
			quando i dati verranno caricati saranno salvati nel routing nell'oggetto server
			*/
			{ path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactiveGuard], }
		]
	},

	/* Il componente server adesso è diventato un bel componente da caricare,(peso, al suo interno ne ha altri 2).
	adesso i server sono raggrupati per poter navigare le rotte figlie, dobbiamo inserire il tag html <router-outer>,
	all'interno del componente server.
	*/

	/* canActivate: [AuthGuard] in questo modo dentro il servizio AuthGuard verra richiamato il metodo canActived e 
	successivamente che ci fornira i dati necessari per capire se l'utente puo navigare o meno quella pagina, protegge 
	la rotta dell'elemento padre e le rotte figlie  

	canActivateChild: [AuthGuard], puoi dichiarare un array con tutti i servizi che verificano se puoi effetivamente 
	andare su una rotta figlia, come per canActivate il servizio deve avere un metodo canActivatedChild che verra richaiamto
	quando andiamo sulla rotta figlia. 	
	*/

	// { path: 'not-found', component: PageNotFoundComponent },
	{ path: 'not-found', component: ErrorPageComponent, data: { message: 'Page not found!' } },
	{ path: '**', redirectTo: 'not-found' },
	/* con ** indichiamo i percorsi jolly dove andranno tutti i percorsi sconosciuti, una volta raggiunta questa riga
	in questo modo è possibile coprire le rotte che non si stanno percorrendo
	con redicrectTo, andiamo ad effettuare il redirect ad un altra rotta 
	l'oridne delle rotte è importantissimo perchè la prima che matcha sarà quella dovre andremo,
	Assicurati che la rotta generica sia sempre l'ultima dell'array, perche le rotte vengono
	analizzate dall'alto verso il basso, quindi se la rotta generica di errore fosse all'inizio adremo 
	a essere reindirizzati sempre nel componente page-not-found
	*/

	/* 
	{ path: '', redirectTo: 'not-found', pathMatch: 'full' },
	prendiamo in esempio la rotta su, angular funziona come strategia di default con prefix, 
	quindi verificherà che il percorso che hai specificato nell'url inizia con il percorso specificato nella rotte.
	Normalemente ogni percorso inizia con ''(Importante: senza spazi è semplicemente 'nothing').
	Per risolvere questo problema dobbiamo usare il patchMatch a 'full' effettuando il redirect solo quando il patch e le rotte 
	fanno un match completo. 
	*/



	/*
	route-gaurds: è una funzionalita logica del codice, viene esucato prima che il roting viene eseguito o quando lasciamo una pagina.
	Supponiamo di voler concede l'accesso all'utente alla pagina dei server solo se l'utente è loggato. 
	Si fa aggiungendo un nuovo file di guards, in questo caso auth-gaurd.service.ts 
	
	*/

]

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
		/*
		// RouterModule.forRoot(appRoutes, { useHash: true })
		alcuni web server potrebbero processo la rotta vuota '' e il server rispondere 404 senza dare la possibilità ad angular 
		di processare le rotte, in questo caso dobbiamo attivare il secondo parametro di RouterModule.forRoot, cioe un oggetto 
		{ useHash:true }, che aggiunge un # dopo il nome del dominio, esempio "dominio/#/" , la parte prima di # sara elaborata 
		dal server e quella dopo da angular.
		*/
	],
	exports: [
		RouterModule
		/*
		negli export ci vanno i pezzi che devono essere esposti di questo modulo da poter utilizzare in altri moduli
		*/
	]
})
export class AppRoutingModule {

}
/*
per buoan pratica e meglio spostare il file di routing in un file separato, 
 */