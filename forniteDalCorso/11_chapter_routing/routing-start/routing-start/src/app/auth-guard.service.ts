import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable(

)
export class AuthGuard implements CanActivate, CanActivateChild {

	constructor(private authService: AuthService, private router: Router) { }

	/*
	è un servizio usato come guardia delle rotte nota che nel nome della classe service non è stato mantenuto
	CanActivate è un interfaccia di aunglua che ci consente di di usare il metodo canActive che ha 2 argoemnti 
	router e sate 
	*/
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		return this.authService.isAuthenticated().then((autenticated: boolean) => {
			if (autenticated) {
				return true
			} else {
				this.router.navigate(['/'])
				return false
			}

		})
	}

	canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		return this.canActivate(childRoute, state)
	}

}